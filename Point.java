public class Point extends Sujet {
    protected int x, y;

    public Point(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    
    
    public void metsAJour(String attributModifie,Object nouvelleValeur){
    for (int i=0; i<observateurs.size(); i++){ 
          Observateur ob = observateurs.get(i);
          ob.metsAJour(attributModifie, nouvelleValeur); 
    } 
}

}
